* MNIST

*mnist* is a package for [[http://www.r-project.org/][R]] that provides easy access to the [[http://yann.lecun.com/exdb/mnist/][MNIST database of handwritten digits]].

** Use

#+begin_src R
  X     <- getMNISTData("trainimages")
  y     <- getMNISTData("trainlabels")
  Xtest <- getMNISTData("testimages")
  ytest <- getMNISTData("testlabels")
#+end_src

   
** License

The R source code for *mnist* is licensed under [[http://www.gnu.org/licenses/gpl.html][GPL-3.0]].  

[[http://yann.lecun.com/][Yann LeCun]] (Courant Institute, NYU) and [[http://web.me.com/corinnacortes/work/Home.html][Corinna Cortes]] (Google
Labs, New York) hold the copyright of [[http://yann.lecun.com/exdb/mnist][MNIST]] dataset, which is a
derivative work from original NIST datasets. [[http://yann.lecun.com/exdb/mnist][MNIST]] dataset is made
available under the terms of the [[http://creativecommons.org/licenses/by-sa/3.0/][Creative Commons Attribution-Share
Alike 3.0]] license.
